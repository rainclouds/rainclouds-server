def TOOLCHAIN_CONFIG

pipeline {
    agent {
        label '!windows'
    }

    environment {
        BUILDING_RC_SERVER=1
        LFS="${WORKSPACE}/${RC_SERVER_VERSION}/lfs"
        LC_ALL="POSIX"
        LFS_TGT = sh(returnStdout: true, script: 'echo "$(uname -m)-lfs-linux-gnu"')
        LFS_GCC_TGT = sh(returnStdout: true, script: 'echo "$(uname -m)-lfs-linux-gnu-gcc"')
        ARCHITECTURE = sh(returnStdout: true, script: 'echo "$(uname -m)"')
        CONFIG_SITE="${LFS}/usr/share/config.site"
        MAKEFLAGS=" -j8"
        PATH="${LFS}/tools/bin:/usr/share/Modules/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/sbin:/usr/bin:/bin"
    }

    stages {
        stage('prepare-environment') {
            steps {
                cleanWs()
                echo "path: ${PATH}"
                echo "params: ${params}"
                script {
                    withFileParameter('TOOLCHAIN_CONFIG_FILE') {
                        TOOLCHAIN_CONFIG = readJSON file: TOOLCHAIN_CONFIG_FILE
                        echo "toolchain_config: ${TOOLCHAIN_CONFIG}"
                    }
                }
                sh "#!/bin/bash -xe"
            }
        }
        stage('prepare-filesystem') {
            steps {
                echo "[Stage: prepare-filesystem] Preparing LFS File System for Rain Clouds ${RC_SERVER_VERSION}..."
                echo "LFS Directory: ${LFS}"
                sh "mkdir -vp ${LFS}/{etc,var,tmp,opt,sources,tools,boot/efi,home} ${LFS}/usr/{bin,lib,sbin,src}"
                dir("${LFS}") {
                    sh "ln -sv ./usr/bin ./bin && ln -sv ./usr/sbin ./sbin && ln -sv ./usr/lib ./lib"
                    sh '[ $ARCHITECTURE = x86_64 ] && mkdir -pv ./usr/lib64 && ln -sv ./usr/lib64 ./lib64 || :'
                }
            }
        }
        stage('fetch-sources') {
            steps {
                echo '[Stage: fetch-sources] Fetching sources to build temporary toolchain and core packages...'
                script {
                    TOOLCHAIN_CONFIG.each {key, value ->
                        sh '[ ' + "${TOOLCHAIN_CONFIG[key]['vcs-engine']}" + ' = mercurial ] && hg clone' + " ${TOOLCHAIN_CONFIG[key]['repository']} ${LFS}/sources/${key} || :"
                        sh '[ ' + "${TOOLCHAIN_CONFIG[key]['vcs-engine']}" + ' = git ] && git clone --depth 1 --branch ' + "${TOOLCHAIN_CONFIG[key]['release-tag']} ${TOOLCHAIN_CONFIG[key]['repository']} ${LFS}/sources/${key} || :"
                    }
                }
            }
        }
        stage('build-toolchain') {
            steps {
                echo '[Stage: build-toolchain] Building temporary toolchain to construct LFS system...'
                // Build: BinUtils
                echo "=========== Building binutils: ${LFS}/sources/binutils ==========="
                sh "mkdir -pv ${LFS}/sources/binutils/build"
                dir("${LFS}/sources/binutils/build") {
                    sh '../configure --prefix=$LFS/tools --with-sysroot=$LFS --target=$LFS_TGT --disable-nls --enable-gprofng=no --disable-werror'
                    sh "make${MAKEFLAGS}"
                    sh "make install"
                }
                echo "=========== Completed Building BinUtils ==========="

                // Build: GCC
                echo "=========== Building gcc: ${LFS}/sources/gcc ==========="
                dir("${LFS}/sources/gcc") {
                    // Link source dependencies for build to repositories in /sources folder
                    sh "ln -sv ../mpfr ./mpfr && ln -sv ../gmp ./gmp && ln -sv ../mpc ./mpc"
                    //On x86_64 hosts, set the default directory name for 64-bit libraries to “lib”: 
                    sh '[ $ARCHITECTURE = x86_64 ] && sed -e "/m64=/s/lib64/lib/" -i.orig gcc/config/i386/t-linux64 || :'
                    // Create the build directory and builds GCC
                    sh "mkdir -pv build"
                    dir("./build") {
                        // Configure and build gcc
                        sh '../configure --prefix=$LFS/tools --with-sysroot=$LFS --target=$LFS_TGT --with-glibc-version=2.38 --with-newlib --without-headers --enable-default-pie --enable-default-ssp --disable-nls --disable-shared --disable-multilib --disable-threads --disable-libatomic --disable-libgomp --disable-libquadmath --disable-libssp --disable-libvtv --disable-libstdcxx --enable-languages=c,c++'
                        sh "make${MAKEFLAGS}"
                        sh "make install"
                    }
                    echo "path: ${PATH}"
                    // Create a full limits.h file by joining limitx.h, glimits.h, and limity.h. Save that concatted header to the LFS filesystemced
                    sh 'cat gcc/limitx.h gcc/glimits.h gcc/limity.h > `dirname $($LFS_GCC_TGT -print-libgcc-file-name)`/include/limits.h'
                }
                echo "=========== Completed Building gcc! ==========="

                // Build: Linux API Headers
                echo "=========== Building Linux Kernel: ${LFS}/sources/linux ==========="
                dir("${LFS}/sources/linux") {
                    sh "make mrproper"
                    sh "make headers"
                    sh "find usr/include -type f ! -name '*.h' -delete"
                    sh 'cp -rv usr/include $LFS/usr'
                }
                echo "=========== Completed Building Linux Kernel!==========="

                // Build: GLibc
                echo "=========== Building glibc: ${LFS}/sources/glibc ==========="
                dir("${LFS}/sources/glibc") {
                    sh "pwd"
                }
                echo "=========== Completed Building glibc! ==========="
            }
        }
        stage('Results') {
            steps {
                echo '[Stage: Done!]'
            }
        }
    }
}